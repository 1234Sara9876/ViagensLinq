﻿namespace ViagensLinq
{
    using LivrariaClasses;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class Criar : Form
    {
        ListaDeViagens listaCopiada = new ListaDeViagens();
        public Criar(ListaDeViagens Packs)
        {
            InitializeComponent();
            listaCopiada = Packs;
            tbIDPack.Text = listaCopiada.GeraIdPack().ToString();//este seria um método de componente, mas tem de ficar embutido no construtor do formulário para aparecer assim que este é mostrado

        }
      


 
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(rtbDesc.Text))
            {
                MessageBox.Show("Tem que inserir a descriçao da viagem");
                return;
            }
            if (udPreco.Value <= 0)
            {
                MessageBox.Show("Introduza o preço do novo pack de viagem");
                return;
            }
            Pack recente = new Pack { IdPack = listaCopiada.GeraIdPack(), Descricao = rtbDesc.Text, Preco = udPreco.Value };
            ListaDeViagens.addpack(recente);
            MessageBox.Show("Pack de viagem gravado com sucesso");
            Hide();
        }
    }
}