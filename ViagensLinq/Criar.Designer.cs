﻿namespace ViagensLinq
{
    partial class Criar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbIDPack = new System.Windows.Forms.TextBox();
            this.rtbDesc = new System.Windows.Forms.RichTextBox();
            this.udPreco = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.udPreco)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Pack";
            // 
            // tbIDPack
            // 
            this.tbIDPack.Location = new System.Drawing.Point(136, 29);
            this.tbIDPack.Name = "tbIDPack";
            this.tbIDPack.Size = new System.Drawing.Size(40, 20);
            this.tbIDPack.TabIndex = 1;
            // 
            // rtbDesc
            // 
            this.rtbDesc.Location = new System.Drawing.Point(26, 66);
            this.rtbDesc.Name = "rtbDesc";
            this.rtbDesc.Size = new System.Drawing.Size(265, 124);
            this.rtbDesc.TabIndex = 2;
            this.rtbDesc.Text = "";
            // 
            // udPreco
            // 
            this.udPreco.DecimalPlaces = 2;
            this.udPreco.Location = new System.Drawing.Point(171, 214);
            this.udPreco.Name = "udPreco";
            this.udPreco.Size = new System.Drawing.Size(85, 20);
            this.udPreco.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Image = global::ViagensLinq.Properties.Resources.if_Close_Icon_1398919;
            this.button2.Location = new System.Drawing.Point(281, 268);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(37, 40);
            this.button2.TabIndex = 5;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Image = global::ViagensLinq.Properties.Resources.if_Tick_Mark_1398911;
            this.button1.Location = new System.Drawing.Point(235, 268);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 40);
            this.button1.TabIndex = 4;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Criar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 303);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.udPreco);
            this.Controls.Add(this.rtbDesc);
            this.Controls.Add(this.tbIDPack);
            this.Controls.Add(this.label1);
            this.Name = "Criar";
            this.Text = "Criar";
            ((System.ComponentModel.ISupportInitialize)(this.udPreco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbIDPack;
        private System.Windows.Forms.RichTextBox rtbDesc;
        private System.Windows.Forms.NumericUpDown udPreco;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}