﻿namespace ViagensLinq
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using LivrariaClasses;

    public partial class Ver : Form
    {
        public  List<Pack> ListaDePacks { get; set; }
        public Ver(ListaDeViagens Packs)
        {
          
            InitializeComponent();
            ListaDePacks = ListaDeViagens.getLista();
     


            foreach (var pack in ListaDePacks)
            {
                dgView.Rows.Add(pack.IdPack, pack.Descricao, pack.Preco);
            }
        }
      

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
