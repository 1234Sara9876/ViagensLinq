﻿namespace ViagensLinq
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using LivrariaClasses;

    public partial class Editar : Form
    {
        ListaDeViagens listaCopiada = new ListaDeViagens();

        List<Pack> listaeditar = new List<Pack>();
        Pack editado = new Pack();
        int index = 0;

        public Editar(ListaDeViagens Packs)
        {
            InitializeComponent();
            listaCopiada = Packs;
            listaeditar = ListaDeViagens.getLista();
            tbIDPack2.Text = listaeditar[index].IdPack.ToString();
            richTextBox1.Text = listaeditar[index].Descricao.ToString();
            numericUpDown1.Value = listaeditar[index].Preco;
        }
        //index = 0;
        //if (listaCopiada.Count > 0)
        //{
        //    MostraLista();
        //}
        //else
        //{
        //    MessageBox.Show("A lista não tem packs de viagens; tem de sair e criar um pack");
        //}
        //        }
        //        private void btForward_Click(object sender, EventArgs e)
        //        {
        //            if (index < listaCopiada.Count - 1)
        //            {
        //                index++;
        //                MostraLista();
        //            }
        //            else
        //            {
        //                MessageBox.Show("A lista não tem mais itens");
        //            }
        //        }

        //        private void btBack_Click(object sender, EventArgs e)
        //        {
        //            if (index > 0)
        //            {
        //                index--;
        //                MostraLista();
        //            }
        //            else
        //            {
        //                MessageBox.Show("A lista não tem mais itens");
        //            }
        //        }

        private void MostraLista()
        {
           /* tbIDPack2.DataSource = listaCopiada;
            tbIDPack2.DisplayMember = editado.IdPack.ToString();

          
            rtbDesc2.Text = listaCopiada[index].Descricao;
            udPreco2.Value = listaCopiada[index].Preco;
            */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            index--;
            if (index >= 0 && index + 1 < listaeditar.Capacity - 1)
            {
                
                tbIDPack2.Text = listaeditar[index].IdPack.ToString();
                richTextBox1.Text = listaeditar[index].Descricao.ToString();
                numericUpDown1.Value = listaeditar[index].Preco;
            }
            else
            {
                index++;
            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            index++;
            if (index >= 0 && index +1 < listaeditar.Capacity -1)
            {
               
                tbIDPack2.Text = listaeditar[index].IdPack.ToString();
                richTextBox1.Text = listaeditar[index].Descricao.ToString();
                numericUpDown1.Value = listaeditar[index].Preco;
            }
            else
            {
                index--;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
         
             listaeditar[index].Descricao = richTextBox1.Text;
             listaeditar[index].Preco = numericUpDown1.Value;
             MessageBox.Show("Pack de viagem alterado com sucesso");
             Close();
        }

        //        private void btGravar_Click(object sender, EventArgs e)
        //        {
        //            for (int i = 0; i < listaCopiada.Count; i++)
        //            {
        //                if (listaCopiada[i].IdPack == Convert.ToInt16(tbIDPack2.Text))
        //                {
        //                    listaCopiada[i].Descricao = rtbDesc2.Text;
        //                    listaCopiada[i].Preco = udPreco2.Value;
        //                    MessageBox.Show("Pack de viagem alterado com sucesso");
        //                    Close();
        //                }

        //            }
        //        }
    }
}
