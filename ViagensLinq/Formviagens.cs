﻿using LivrariaClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ViagensLinq
{
    public partial class Formviagens : Form
    {
        ListaDeViagens Packs;

        private Criar fCriar;
        private Ver fVer;
        private Editar fEditar;
        private Remover fRemover;

        public Formviagens()
        {
            InitializeComponent();
            Packs = new ListaDeViagens();
            Packs.LoadPacks();
        }

        #region Métodos dos componentes

        private void tsmAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Projeto PackViagens - Sara Cardoso em 8/10/2017, V 1.0.0");
        }

        private void btCreate_Click(object sender, EventArgs e)
        {
            fCriar = new Criar(Packs);
            fCriar.Show();
        }

        private void btRead_Click(object sender, EventArgs e)
        {
            fVer = new Ver(Packs);
            fVer.Show();
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            fEditar = new Editar(Packs);
            fEditar.Show();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
           /* 
                fRemover = new Remover(Packs);
                fRemover.Show();
            */
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            if (Packs.GravarViagens())
            {
                MessageBox.Show("Gravação bem sucedida");
            }
            Close();
        }
        #endregion

        private void btFechar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btCreate_Click_1(object sender, EventArgs e)
        {
            fCriar = new Criar(Packs);
            fCriar.Show();
        }

        private void btRead_Click_1(object sender, EventArgs e)
        {
            fVer = new Ver(Packs);
            fVer.Show();
        }

        private void btDelete_Click_1(object sender, EventArgs e)
        {
            fRemover = new Remover();
            fRemover.Show();
              
        }

        private void btUpdate_Click_1(object sender, EventArgs e)
        {
            fEditar = new Editar(Packs);
            fEditar.Show();
        }

       
    }
}

