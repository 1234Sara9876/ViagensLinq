﻿namespace ViagensLinq
{
    partial class Ver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgView = new System.Windows.Forms.DataGridView();
            this.ID_Pack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição_Viagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preço_Viagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.SuspendLayout();
            // 
            // dgView
            // 
            this.dgView.AllowUserToDeleteRows = false;
            this.dgView.AllowUserToOrderColumns = true;
            this.dgView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Pack,
            this.Descrição_Viagem,
            this.Preço_Viagem});
            this.dgView.Location = new System.Drawing.Point(12, 12);
            this.dgView.Name = "dgView";
            this.dgView.ReadOnly = true;
            this.dgView.Size = new System.Drawing.Size(461, 303);
            this.dgView.TabIndex = 0;
            // 
            // ID_Pack
            // 
            this.ID_Pack.HeaderText = "ID_Pack";
            this.ID_Pack.Name = "ID_Pack";
            this.ID_Pack.ReadOnly = true;
            // 
            // Descrição_Viagem
            // 
            this.Descrição_Viagem.HeaderText = "Descrição_Viagem";
            this.Descrição_Viagem.Name = "Descrição_Viagem";
            this.Descrição_Viagem.ReadOnly = true;
            // 
            // Preço_Viagem
            // 
            this.Preço_Viagem.HeaderText = "Preço_Viagem";
            this.Preço_Viagem.Name = "Preço_Viagem";
            this.Preço_Viagem.ReadOnly = true;
            this.Preço_Viagem.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // button1
            // 
            this.button1.Image = global::ViagensLinq.Properties.Resources.Close;
            this.button1.Location = new System.Drawing.Point(442, 333);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(41, 38);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Ver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 373);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgView);
            this.Name = "Ver";
            this.Text = "Ver";
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Pack;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição_Viagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preço_Viagem;
        private System.Windows.Forms.Button button1;
    }
}