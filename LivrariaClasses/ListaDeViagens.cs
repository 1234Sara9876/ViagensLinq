﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LivrariaClasses
{
    public class ListaDeViagens
    {
        /*Criei a lista em static*/
        public static List<Pack> ListaDePacks { get; set; }
        /*Criei metodos static*/
        public static List<Pack> getLista()
        {
            return ListaDePacks;
        }

        public static void addpack(Pack p)
        {
            ListaDePacks.Add(p);
        }
        






        public List<Pack> LoadPacks()
        {
            ListaDePacks = new List<Pack>();
            if (!CarregarViagens(ListaDePacks))
            {
                ListaDePacks.Add(new Pack { IdPack = 1, Descricao = "Sete dias na Patagónia", Preco = 402.50m });
                ListaDePacks.Add(new Pack { IdPack = 2, Descricao = "Catorze dias na China", Preco = 3500.00m });
            }
            return ListaDePacks;
        }

        public List<Pack> Somar(Pack recente)
        {
            ListaDePacks = new List<Pack>();
            ListaDePacks.Add(recente);
            return ListaDePacks;
        } 
        public int GeraIdPack()
        {
            if (ListaDePacks.Count > 0)
            {
                return ListaDePacks [ListaDePacks.Count - 1].IdPack + 1;
            }
            else
            {
                return 1;
            }
        }

        public bool CarregarViagens(List<Pack> ListaDePacks)
        {
            string ficheiro = @"Packs.txt";
            StreamReader sr;
            try
            {
                if (File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);
                    string linha = "";
                    while ((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[3];
                        campos = linha.Split(';');
                        var k = new Pack
                        {
                            IdPack = Convert.ToInt32(campos[0]),
                            Descricao = campos[1],
                            Preco = Convert.ToDecimal(campos[2])
                        };
                        ListaDePacks.Add(k);
                    }
                    sr.Close();
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }
        public bool GravarViagens()
        {
            string ficheiro = @"Packs.txt";
            //streamwriter tem como argumentos (string path, bool append), se append fôr false cria o ficheiro, se append fôr true faz overwrite
            StreamWriter sw = new StreamWriter(ficheiro, false);
            //try-catch lança uma excepção, se o ficheiro n ficar gravado
            try
            {
                if (!File.Exists(ficheiro))
                    sw = File.CreateText(ficheiro);
                foreach (var viagem in ListaDePacks)
                {
                    string linha = string.Format("{0};{1};{2}", viagem.IdPack,
                    viagem.Descricao, viagem.Preco);
                    sw.WriteLine(linha);
                }
                sw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }

    }
}
