﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivrariaClasses
{
    public class Pack
    {
        public int IdPack { get; set; }
        public string Descricao { get; set; }
        public decimal Preco { get; set; }
        public string PackViagem => $"{IdPack} {Descricao} {Preco}";



       
    }

   
}
